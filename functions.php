<?php
/**
 * Describe child theme functions
 *
 * @package SpiderMag
 * @subpackage Black Magazine
 * 
 */

 if ( ! function_exists( 'black_magazine_setup' ) ) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function black_magazine_setup() {

        /*
        * Make theme available for translation.
        * Translations can be filed in the /languages/ directory.
        * If you're building a theme based on Black Magazine, use a find and replace
        * to change 'black-magazine' to the name of your theme in all the template files.
        */
        load_theme_textdomain( 'black-magazine', get_template_directory() . '/languages' );

        
        
        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'custom-header');
        add_theme_support( 'header-text');
        
        /*
        * Let WordPress manage the document title.
        * By adding theme support, we declare that this theme does not use a
        * hard-coded <title> tag in the document head, and expect WordPress to
        * provide it for us.
        */
        add_theme_support( 'title-tag' );
        
        $black_magazine_theme_info = wp_get_theme();
        $GLOBALS['black_magazine_version'] = $black_magazine_theme_info->get( 'Version' );
    }
endif;
add_action( 'after_setup_theme', 'black_magazine_setup' );


/**
 * Enqueue child theme styles and scripts
*/
function black_magazine_scripts() {
    
    global $black_magazine_version;
    wp_dequeue_style( "spidermag-style" );
    wp_enqueue_style( 'spidermag-parent-style', trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'style.css', array(), esc_attr( $black_magazine_version ) );
    
    wp_enqueue_style( 'black-magazine-style', get_stylesheet_uri(), esc_attr( $black_magazine_version ) );
    wp_add_inline_style( 'black-magazine-style', black_magazine_strip_whitespace(black_magazine_dynamic_css()) );
    wp_enqueue_style( 'spidermag-responsive', get_template_directory_uri().'/assets/css/responsive.css' );
}
add_action( 'wp_enqueue_scripts', 'black_magazine_scripts', 20 );


if ( ! function_exists( 'black_magazine_child_options' ) ) {
    function black_magazine_child_options( $wp_customize ) {
        // Primary Color.
        $wp_customize->add_setting('black_mag_primary_color', array(
            'default' => '#e74c3c',
            'sanitize_callback' => 'sanitize_hex_color',
        ));

        $wp_customize->add_control('black_mag_primary_color', array(
            'type' => 'color',
            'label' => esc_html__('Primary Color', 'black-magazine'),
            'section' => 'colors',
        ));

        $wp_customize->add_setting('black_mag_enable_dark_mode', array(
            'default' => true,
            'sanitize_callback' => 'spidermag_checkbox_sanitize',	//done
        ));
    
        $wp_customize->add_control('black_mag_enable_dark_mode', array(
            'type' => 'checkbox',
            'label' => esc_html__('Dark Mode', 'black-magazine'),
            'section' => 'colors'
        ));


    }
}
add_action( 'customize_register' , 'black_magazine_child_options', 11 );

/**
 * Dynamic Style from parent
 */

// add_filter( 'spidermag_dynamic_css', 'black_magazine_dynamic_css', 100 );
function black_magazine_dynamic_css(){
    $dynamic_css = '';
    $primary_color = get_theme_mod('black_mag_primary_color');
    if($primary_color){

        $dynamic_css .= "
            input[type=\"submit\"],
            .lSAction>a,
            .search-container,
            .scrollup,
            .title-icon,
            .box-header-nav .main-menu .children>.page_item.current_page_item>a, .box-header-nav .main-menu .sub-menu>.menu-item.current-menu-item>a,
            .box-header-nav .main-menu .children>.page_item:hover>a, .box-header-nav .main-menu .children>.page_item.focus>a, .box-header-nav .main-menu .sub-menu>.menu-item:hover>a, .box-header-nav .main-menu .sub-menu>.menu-item.focus>a,
            .spider-home.spider-home-active.ion-home, .navbar-inverse .main-nav li.current-menu-item a,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li a,
            a:hover .thumb-box span,
            .toggle-btn-header,
            .post-box-wrapper::before, .post-box-wrapper::after,
            .color-switcher ul li a.active,
            .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,
            .toggle-search{
                background-color: $primary_color;
            }

            a,
            .author-info h4,
            .color-switcher ul li a,
            .read-more:hover,
            a:hover, a:focus,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-account .woocommerce-MyAccount-navigation ul li:hover a,
            .woocommerce-MyAccount-content a:hover, .woocommerce-MyAccount-content a:hover,
            .widget a:hover, .widget a:hover::before, .widget li:hover::before,
            .text-danger,.icon-news,.ticker-content, .ticker-content a, .read-more, .read-more:focus,
            .widget_archive a::before, .widget_categories a::before, .widget_recent_entries a::before, .widget_meta a::before, .widget_recent_comments li::before, .widget_rss li:before, .widget_pages li:before, .widget_nav_menu li:before, .widget_product_categories a:before{
                color: $primary_color;
            }

            .color-switcher ul li a,
            .color-switcher ul li a.active,
            .masonry-item:hover,
            .post-box-wrapper,
            blockquote,
            footer,
            input[type=\"submit\"],
            .lSGallery li a::before,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li a,
            .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,
            .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-account .woocommerce-MyAccount-navigation ul li:hover a,
            .woocommerce-account .woocommerce-MyAccount-content,
            .navbar-inverse{
                border-color: $primary_color;
            }
            .read-more svg path{
                fill: $primary_color;
            }
        ";
    }

    return $dynamic_css;
    
}

function black_magazine_strip_whitespace($css) {
    $replace = array(
        "#/\*.*?\*/#s" => "", // Strip C style comments.
        "#\s\s+#" => " ", // Strip excess whitespace.
    );
    $search = array_keys($replace);
    $css = preg_replace($search, $replace, $css);

    $replace = array(
        ": " => ":",
        "; " => ";",
        " {" => "{",
        " }" => "}",
        ", " => ",",
        "{ " => "{",
        ";}" => "}", // Strip optional semicolons.
        ",\n" => ",", // Don't wrap multiple selectors.
        "\n}" => "}", // Don't wrap closing braces.
        "} " => "}", // Put each rule on it's own line.
    );
    $search = array_keys($replace);
    $css    = str_replace($search, $replace, $css);

    return trim($css);
}

/**
 * Enqueue required scripts/styles for customizer panel
 *
 * @since 1.0.0
 *
 */
function black_magazine_customize_scripts(){
	wp_enqueue_script('black-magazine-customizer', get_stylesheet_directory_uri(  ). '/js/admin.js', array('jquery', 'customize-controls'), true);
}
add_action('customize_controls_enqueue_scripts', 'black_magazine_customize_scripts');

add_filter( 'body_class', 'black_magzaine_remove_body_class', 100 );
function black_magzaine_remove_body_class( $classes ) { 
    if( get_theme_mod('black_mag_enable_dark_mode', true) == false ){
        $remove_classes = ['sp-blackmode'];
        $classes = array_diff($classes, $remove_classes);
    }
    return $classes;    
}

/**********************************************************************************
 * Register widget area.
 **********************************************************************************
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
if ( ! function_exists( 'black_magazine_widgets_init' ) ) {
   
    function black_magazine_widgets_init() {
        
        register_sidebar( array(
            'name'          => esc_html__( 'Right Sidebar Widget', 'black-magazine' ),
            'id'            => 'sidebar-1',
            'description'   => '',
            'before_widget' => '<aside id="%1$s" class="main-title-outer-wrap clearfix widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<div class="main-title-outer"><div class="widget-title main-title">',
            'after_title'   => '</div></div>',
        ));
    }
}
add_action( 'widgets_init', 'black_magazine_widgets_init' );
if(!function_exists('black_magazine_colored_category')){
    function black_magazine_colored_category($p){
        global $post;
        $post = $p;
        $categories = get_the_category();
        $separator = '&nbsp;';
        $output = '';
        if($categories) {
            $output .= '<span class="cat-links layout-2">';
            foreach($categories as $category) {
                $color_code = spidermag_category_color( get_cat_id( $category->cat_name ) );
                if (!empty($color_code)) {
                    $id = uniqid(5); 
                    $output .= '<a id="abcd'.$id.'" href="'.esc_url( get_category_link( $category->term_id ) ).'" rel="category tag">'.esc_html( $category->cat_name ).'</a>'.$separator;
                    $output .= "<style>
                        #abcd{$id}{
                            color: $color_code;
                        }
                        #abcd{$id}:before{
                            background-color: $color_code;
                        }

                        </style>";
                } else {
                    $output .= '<a href="'.esc_url( get_category_link( $category->term_id ) ).'"  rel="category tag">'.esc_html( $category->cat_name ).'</a>'.$separator;
                }
            }
            $output .='</span>';
            echo trim( $output, $separator );
        }
    }
}

if(!function_exists('black_magazine_colored_category2')){
    function black_magazine_colored_category2($p){
        global $post;
        $post = $p;
        spidermag_colored_category();
    }
}

require get_stylesheet_directory() . '/inc/header.php';
require get_stylesheet_directory() . '/inc/widgets/widget-blogs.php';
require get_stylesheet_directory() . '/inc/widgets/widget-blogs2.php';
require get_stylesheet_directory() . '/inc/widgets/widget-blogs3.php';
require get_stylesheet_directory() . '/inc/widgets/widget-blogs4.php';
require get_stylesheet_directory() . '/inc/widgets/widget-sidebar.php';

function black_magazine_startSession() {
    if(!session_id()) {
        session_start();
    }
    if(isset($_GET['switch-color'])){
        $color = $_GET['switch-color'];
        $_SESSION['sp-color'] = $color;
        if($color == 'black'){
            if(get_theme_mod('black_mag_enable_dark_mode') == false){
                set_theme_mod('black_mag_enable_dark_mode', 1);
                set_theme_mod('background_color', sanitize_hex_color('161616'));
            }
        }else{
            if(get_theme_mod('black_mag_enable_dark_mode') == true){
                set_theme_mod('black_mag_enable_dark_mode', 0);
                set_theme_mod('background_color', sanitize_hex_color('ffffff'));
            }
        }
    }else{
        $_SESSION['sp-color'] = 'black';
    }
}

add_action('init', 'black_magazine_startSession', 10);


add_action('wp_footer', 'black_magazine_hidden_sidebar', 100);
function black_magazine_hidden_sidebar(){
    ?>
    <div class="menu-modal sidebar-modal cover-modal header-footer-group" data-modal-target-string=".sidebar-modal" data-modal-target-show="relative">
        <div class="menu-modal-inner modal-inner">
            <div class="menu-wrapper section-inner">
                <div class="menu-top">

                    <button class="toggle close-nav-toggle close-nav-toggle-sidebar" data-toggle-target=".sidebar-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".sidebar-modal-btn">
                        <span class="toggle-text"><?php esc_html_e( 'Close', 'black-magazine' ); ?></span>
                        <i class="fa fa-times"></i>
                    </button><!-- .nav-toggle -->

                    <div class='sparkle-tab-wrap'>
                        <div class="tab-content">
                            <?php get_sidebar('rightsidebar'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    $color = $_SESSION['sp-color'];
    $light_active = $dark_active = '';
    if($color == 'black') { $dark_active = 'active'; }
    elseif($color== 'white') { $light_active = 'active'; }
    ?>

    <div class="color-switcher">
        <ul>
            <li class="light-mode"><a href="?switch-color=white" class="<?php echo esc_attr($light_active); ?>"> <span><?php echo esc_html('Light', 'black-magazine'); ?></span> </a></li>

            <li class="dark-mode"><a href="?switch-color=black" class="<?php echo esc_attr($dark_active); ?>"> <span><?php echo esc_html('Dark', 'black-magazine'); ?></span> </a></li>
        </ul>
    </div>

    <?php
}