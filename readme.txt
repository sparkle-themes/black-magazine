=== Black Magazine ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 5.9
Requires PHP: 7.0
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Black Magazine is a ultra fast clean and best  responsive free WordPress magazine, spidermag specially designed for magazine, newspaper, news portals and editorial style websites.

== Description ==

Black Magazine is a ultra fast clean and best  responsive free WordPress magazine, spidermag specially designed for magazine, newspaper, news portals and editorial style websites.  It comes with the flat, minimalist, magazine style homepage Design with post banner slider and latest posts, and different custom widget design layout. Awesome design for categories, tags, archive pages with multiple sidebars and Ads, widgets. the theme has 10+ different drag and drop widget and 10+ widget area that allows adding many section as you want with various beautifully designed layouts suitable for websites. spidermag theme is completely built on customizer which allows you to customize most of the theme settings easily with live previews, spidermag supports many 3rd party plugins, compatible with Jetpack, Contact Form 7, WooCommerce, AccessPress Social Share, AccessPress Social Counter and many more. Official Support Forum : https://www.sparklewpthemes.com/support/ Video : https://www.youtube.com/channel/UCNz4pwcVXsZfVichroSRdKg Full Demo: http://demo.sparklewpthemes.com/spidermag/demos/ and Docs: http://docs.sparklewpthemes.com/spidermag/


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme demo? =

You can check our Theme features at https://demo.sparklewpthemes.com/spidermag/

= Where can I find theme all features ? =

You can check our Theme Demo at https://sparklewpthemes.com/wordpress-themes/black-magazine


== Translation ==

Black Magazine theme is translation ready.


== Copyright ==

Black Magazine WordPress Theme is child theme of appzend, Copyright 2021 Sparkle Themes.
Black Magazine is distributed under the terms of the GNU GPL

== Credits ==

	Images
    All the images are used from http://pxhere.com  under License CC0 Public Domain or self taken are fully GPL compatible.
    https://pxhere.com/en/photo/714535, License CC0 Public Domain
    https://pxhere.com/en/photo/418977, License CC0 Public Domain


== Changelog ==
= 1.0.3 18h Feb 2022 =
* WordPress 5.9 Compatible

= 1.0.2 11th Jul 2021 =
** Offconvas keyword navigation close issue fixed
** Function Prefix

= 1.0.1 9th Jul 2021 =
** 4 Widgets are added
** Black and White Switch
** Design Improved

= 1.0.0 22nd Jun 2021 =
** Initial submit theme on wordpress.org trac.