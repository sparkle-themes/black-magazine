jQuery(document).ready(function ($) {
    
    /**
     * Customizer Option Auto focus
     */
     jQuery('h3.accordion-section-title').on('click', function(){
        var id = $(this).parent().attr('id');
        var is_panel = id.includes("panel");
        var is_section = id.includes("section");
        
        if( is_panel ){
            focus_item = id.replace('accordion-panel-', '');
            console.log(focus_item);
            history.pushState({}, null, '?autofocus[panel]=' + focus_item);
        }
        if( is_section ){
            focus_item = id.replace('accordion-section-', '');
            history.pushState({}, null, '?autofocus[section]=' + focus_item);
        }
    });


    /**
     * Set Dark Mode Color
     * @since 1.0.0
     */
     wp.customize( 'black_mag_enable_dark_mode', function( setting ) {
        setting.bind( function onChange( value ) {
            if (value){
                wp.customize.control( 'background_color' ).setting.set( '#161616' );
                wp.customize.control( 'header_textcolor' ).setting.set( '#fff' );
            }else{
                wp.customize.control( 'background_color' ).setting.set( '#fff' );
                wp.customize.control( 'header_textcolor' ).setting.set( '#161616' );
            }
        } );
    });
});