<?php
/**
 * remove parent actions
 */
add_action( 'init', 'construction_choice_remove_action');
function construction_choice_remove_action() {
    remove_action( 'spidermag_header', 'spidermag_main_header', 20 );
}

if ( ! function_exists( 'black_magazine_main_header' ) ) {
    /**
     * Skip links
     * @since  1.0.0
     * @return void
     */
    function black_magazine_main_header() { ?>
            <div class="sticky-header">
                <div class="container header">
                    <div class="row sm-flex">
                        <div class="col-sm-8 col-md-4">
                            <div class="site-branding flex">
                                <?php
                                    if(esc_attr(get_theme_mod('spidermag_home_icon_display', 1)) == 1):
                                        $home_icon_class = 'spider-home spider-home-active'; ?>
                                        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="<?php echo esc_attr( $home_icon_class ); ?> ion-home">&nbsp;</a>
                                    
                                <?php endif; ?>        

                                <div class="spidermag-logo">
                                    <?php 
                                        if ( function_exists( 'the_custom_logo' ) ) { 
                                            the_custom_logo();
                                        } 
                                    ?>
                                </div>
                                <?php if( display_header_text() ): ?>
                                <div class="spidermag-logotitle">
                                    <h1 class="site-title">
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                            <?php bloginfo( 'name' ); ?>
                                        </a>
                                    </h1>
                                    <?php 
                                        $description = get_bloginfo( 'description', 'display' );
                                        if ( $description || is_customize_preview() ) { ?>
                                            <p class="buzz-site-description site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
                                    <?php } ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div><!-- header main logo section -->

                        <div class="col-sm-8 col-md-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle-target=".header-mobile-menu"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
                                        <span class="sr-only"><?php esc_html_e('Toggle navigation','black-magazine'); ?></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span> 
                                        <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-wrapper flex">
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                                <nav class="box-header-nav main-menu-wapper" aria-label="<?php esc_attr_e( 'Main Menu', 'black-magazine' ); ?>" role="navigation">
                                    <?php
                                        wp_nav_menu( array(
                                                'theme_location'  => 'primary',
                                                'menu'            => 'primary-menu',
                                                'container'       => false,
                                                'container_class' => '',
                                                'container_id'    => '',
                                                'menu_class'      => 'main-menu second nav navbar-nav text-uppercase main-nav',
                                                'fallback_cb' => 'spidermag_menu_default'
                                            )
                                        );
                                    ?>
                                </nav>
                            </div> <!-- collapse navbar-collapse -->

                            <div class="right-side-container">
                                
                                <?php if ( esc_attr( get_theme_mod( 'spidermag_search_icon_in_menu', 1 ) ) == 1 ){ ?>
                                    <a href="javascript:;" class="toggle-search pull-right"><span class="ion-android-search"></span></a>
                                <?php } ?>
                                
                                <a href="javascript:;" class="toggle-btn-header pull-right sidebar-modal-btn" data-toggle-target=".sidebar-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle-sidebar"><span class="ion-android-information"></span></a>

                            </div>
                            </div>
                        </div><!-- header ads section -->
                    </div> <!-- .row -->
                </div><!-- container --><!-- header end -->    

                <div class="<?php if ( esc_attr(get_theme_mod('spidermag_primary_sticky_menu', 1 ) ) == 1){ echo 'nav-search-outer'; } ?>">
                    <div class="search-container ">
                        <div class="container">
                            <form method="get" class="search-form" action="<?php echo esc_url(home_url( '/' )); ?>">
                                <input type="search" name="s" id="search-bar" placeholder="<?php esc_attr_e( 'Type & Hit Enter..', 'black-magazine' ); ?>" value="<?php echo esc_attr(get_search_query()) ?>" autocomplete="off">
                            </form>
                        </div>
                    </div><!-- search end -->
                </div><!-- nav and search end-->
            </div><!-- sticky header end -->
        <?php
    }
}
add_action( 'spidermag_header', 'black_magazine_main_header', 20 );