<?php
/**
 ** Adds black_magazine_blog_widget4 widget.
*/
add_action('widgets_init', 'black_magazine_blog_widget4');
function black_magazine_blog_widget4() {
    register_widget('Black_Mag_Blog_Widget4');
}
class Black_Mag_Blog_Widget4 extends WP_Widget {

    /**
     * Register widget with WordPress.
    */
    public function __construct() {
        parent::__construct(
            'bloack_mag_blog_widget4',  esc_html__( '&nbsp;BM : Blogs 4','black-magazine'), 
            array(
                'description' => esc_html__('A widget that shows category Posts with single large images list view', 'black-magazine')
            )
        );
    }

    /**
     * Helper function that holds widget fields
     * Array is used in update and form functions
    */
    private function widget_fields() {        
        $args = array(
            'type'       => 'post',
            'child_of'   => 0,
            'orderby'    => 'name',
            'order'      => 'ASC',
            'hide_empty' => 1,
            'taxonomy'   => 'category',
        );

        $multi_categories = get_categories( $args );
        $mag_categories_lists = array();
        foreach( $multi_categories as $multi_categorie ) {
            $mag_categories_lists[$multi_categorie->term_id] = $multi_categorie->name;
        }

        $fields = array(   

            'six_block_title' => array(
                'spidermag_widgets_name' => 'six_block_title',
                'spidermag_widgets_title' => esc_html__('Title', 'black-magazine'),
                'spidermag_widgets_field_type' => 'title',
            ),

            'six_block_list_category' => array(
              'spidermag_widgets_name' => 'six_block_list_category',
              'spidermag_mulicheckbox_title' => esc_html__('Select Block Category', 'black-magazine'),
              'spidermag_widgets_field_type' => 'multicheckboxes',
              'spidermag_widgets_field_options' => $mag_categories_lists
            ),

            'six_block_post_order' => array(
                'spidermag_widgets_name' => 'six_block_post_order',
                'spidermag_widgets_title' => esc_html__('Display Posts Order', 'black-magazine'),
                'spidermag_widgets_field_type' => 'select',
                'spidermag_widgets_field_options' => array('desc' => 'Deaccessing Order', 'asc' => 'Accessing Order' )
            )
                 
        );
        return $fields;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        
        $six_block_title         = empty( $instance['six_block_title'] ) ? '' : $instance['six_block_title'];
        $six_block_post_order    = empty( $instance['six_block_post_order'] ) ? 'des ' : $instance['six_block_post_order'];
        $six_block_list_category = empty($instance['six_block_list_category']) ? 0 : $instance['six_block_list_category'];        
        
        $multi_left_cat_id = array();
        if(!empty($six_block_list_category)){
            $multi_left_cat_id = array_keys( unserialize($six_block_list_category));
        }

        $get_grid_list_posts = get_posts( array(
            'posts_per_page'        => 5,
            'post_type'             => 'post',
            'category__in'          => $multi_left_cat_id,
            'order'                 => $six_block_post_order,
            'ignore_sticky_posts'   => true
        ) );
        
        echo $before_widget; ?>
        <div class="container">
            <div class="row">
            <div class="col-lg-16 col-md-16 col-sm-16">
                <?php if( !empty( $six_block_title ) ){ ?>
                <div class="section-title">
                    <h3><?php echo esc_html( $six_block_title ); ?></h3>
                </div>
            
                <?php } ?>
            
            <?php foreach($get_grid_list_posts as $key => $p): setup_postdata( $p ); if($key == 0): ?>
                <div class="read-post-wrapper flex">
                    <div class="big-content-part read-content">

                        <span class="bg-color-txt cat-links">
                            <a href="#">Fashion</a></span>
                        <h3><a href="<?php echo esc_url(get_permalink($p)); ?>"><?php echo esc_html( $p->post_title ); ?></a></h3> 
                        <div class="text-danger sub-info-bordered">
                            <?php spidermag_meta_options( array( 'author','time' ) ); ?>
                        </div>
                        <p><?php echo apply_filters( 'get_the_excerpt', $p->post_excerpt, $p ); ?></p>
                    </div>
                    <div class="read-post-img">
                        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), 'spidermag-main-banner', true); 
                            if($image):
                        ?>
                            <img src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo esc_attr( $p->post_title ); ?> ?>" title="<?php echo esc_attr( $p->post_title ); ?>"/>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                <?php else: ?>

                <div class="col-lg-4 col-md-4 col-sm-8">
                    <div class="img-cont-post icons-txt">
                        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), 'spidermag-main-banner', true); 
                            if($image):
                        ?>
                            <img src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo esc_attr( $p->post_title ); ?> ?>" title="<?php echo esc_attr( $p->post_title ); ?>"/>
                        <?php endif; ?>

                        <div class="post-list">
                        <h5><a href="<?php echo esc_url(get_permalink($p)); ?>"><?php echo esc_html( $p->post_title ); ?></a></h5> 
                        <div class="text-danger sub-info-bordered">
                            <?php spidermag_meta_options( array( 'author','time' ) ); ?>
                        </div>
                        <p><?php echo apply_filters( 'get_the_excerpt', $p->post_excerpt, $p ); ?></p>
                    </div>
                    </div>
                </div>
                <?php endif; endforeach; ?>
            </div>
        </div>
       

    <?php 
        echo $after_widget;
    }
    
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $widget_fields = $this->widget_fields();
        // Loop through fields
        foreach ($widget_fields as $widget_field) {
            extract($widget_field);
            // Use helper function to get updated field values
            $instance[$spidermag_widgets_name] = spidermag_widgets_updated_field_value($widget_field, $new_instance[$spidermag_widgets_name]);
        }

        return $instance;
    }

    public function form($instance) {
        $widget_fields = $this->widget_fields();
        // Loop through fields
        foreach ($widget_fields as $widget_field) {
            // Make array elements available as variables
            extract($widget_field);
            $spidermag_widgets_field_value = !empty($instance[$spidermag_widgets_name]) ? $instance[$spidermag_widgets_name] : '';
            spidermag_widgets_show_widget_field($this, $widget_field, $spidermag_widgets_field_value);
        }
    }
}